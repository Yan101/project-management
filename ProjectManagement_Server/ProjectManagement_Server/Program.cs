﻿using System;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace ProjectManagement_Server
{
    class Program
    {
        private const string APP_PATH = "http://localhost:8080/";

        static void Main()
        {
            ConfigurationServer();
        }

        private static void ConfigurationServer()
        {
            var configuration = new HttpSelfHostConfiguration(APP_PATH);

            configuration.Routes.MapHttpRoute(
                "API Default", "api/{controller}/{action}/{id}", new { id = RouteParameter.Optional });

            using (HttpSelfHostServer server = new HttpSelfHostServer(configuration))
            {
                server.OpenAsync();
                Console.WriteLine("Web API Service started. Press Enter to quit!");
                Console.ReadLine();
            }
        }
    }
}