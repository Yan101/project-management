﻿using System.Collections.Generic;
using ProjectManagement_Server.Models;
using System.Web.Http;
using System.Data.SqlClient;
using System;
using System.Configuration;
using System.Data;

namespace ProjectManagement_Server.Controllers
{
    public class ValuesController : ApiController
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["ProjectManagementConnection"].ConnectionString;

        private List<ProjTimeEmp> projTimeEmp = new List<ProjTimeEmp>();
        private List<TotalTimeEmployee> totalTimeEmployee = new List<TotalTimeEmployee>();
        private List<EmployeesWithoutWorkTime> employeesWithoutWorkTime = new List<EmployeesWithoutWorkTime>();
        private List<AllProjects> allProjects = new List<AllProjects>();
        private List<AllEmployees> allEmployees = new List<AllEmployees>();

        public List<ProjTimeEmp> GetCommonInfoOnProjects()
        {
            string sqlStoredProcedure = "sp_GeneralTimeToProj_NumbeOfEmployeesToProj";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }
                catch (SqlException ex)

                {
                    Console.WriteLine(ex.Message);
                }

                SqlCommand command = new SqlCommand(sqlStoredProcedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int IDProj = reader.GetInt32(0);
                        string nameProj = reader.GetString(1);
                        int generalTime = reader.GetInt32(2);
                        int numbeOfEmployees = reader.GetInt32(3);

                        projTimeEmp.Add(new ProjTimeEmp { ID = IDProj, NameProj = nameProj, GeneralTime = generalTime, NumberOfEmployees = numbeOfEmployees });
                    }
                }
                reader.Close();
            }

            return projTimeEmp;
        }
        public List<TotalTimeEmployee> GetTotalTimeEmployees()
        {
            string sqlStoredProcedure = "sp_TotalSpentTimeEachEmployee";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }
                catch (SqlException ex)

                {
                    Console.WriteLine(ex.Message);
                }

                SqlCommand command = new SqlCommand(sqlStoredProcedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int IDEmployee = reader.GetInt32(0);
                        string fName = reader.GetString(1);
                        int totalTime = reader.GetInt32(2);

                        totalTimeEmployee.Add(new TotalTimeEmployee { ID = IDEmployee, FName = fName, TotalTime = totalTime });
                    }
                }
                reader.Close();
            }
            return totalTimeEmployee;
        }
        public List<EmployeesWithoutWorkTime> GetEmployeesWithoutWorkTime()
        {
            string sqlStoredProcedure = "sp_EmployeesWithoutWorkTime";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }
                catch (SqlException ex)

                {
                    Console.WriteLine(ex.Message);
                }

                SqlCommand command = new SqlCommand(sqlStoredProcedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int IDEmployeeWithOutTime = reader.GetInt32(0);
                        string fName = reader.GetString(1);

                        employeesWithoutWorkTime.Add(new EmployeesWithoutWorkTime { ID = IDEmployeeWithOutTime, FName = fName });
                    }
                }
                reader.Close();
            }
            return employeesWithoutWorkTime;
        }
        public List<AllProjects> GetAllProjects()
        {
            string sqlStoredProcedure = "sp_GetAllProjects";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }

                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                SqlCommand command = new SqlCommand(sqlStoredProcedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string projectName = reader.GetString(1);

                        allProjects.Add(new AllProjects { ID = id, ProjectName = projectName });
                    }
                }
                reader.Close();
            }
            return allProjects;
        }
        public List<AllEmployees> GetAllEmployees()
        {
            string sqlStoredProcedure = "sp_GetAllEmployees";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }

                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                SqlCommand command = new SqlCommand(sqlStoredProcedure, connection);
                command.CommandType = CommandType.StoredProcedure;

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string employeeFName = reader.GetString(1);

                        allEmployees.Add(new AllEmployees { ID = id, EmployeeFirstName = employeeFName });
                    }
                }
                reader.Close();
            }
            return allEmployees;
        }
        public void PostAddWorkTimeEmployees(PostAddWorkTime postAddWorkTime)
        {
            int ID_Project = postAddWorkTime.ID_Project;
            int ID_Employee = postAddWorkTime.ID_Employee;
            int WorkTime = postAddWorkTime.SpentEmployeeTime;

            string PostQuery = "INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES(@ID_Project, @ID_Eployee, @workTime)";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                }

                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                SqlCommand command = new SqlCommand(PostQuery, connection);

                SqlParameter ID_ProjectParamater = new SqlParameter("@ID_Project", ID_Project);
                command.Parameters.Add(ID_ProjectParamater);

                SqlParameter ID_EpmloyeeParameter = new SqlParameter("@ID_Eployee", ID_Employee);
                command.Parameters.Add(ID_EpmloyeeParameter);

                SqlParameter workTimeParameter = new SqlParameter("@workTime", WorkTime);
                command.Parameters.Add(workTimeParameter);

                command.ExecuteNonQuery();
            }
        }
    }
}