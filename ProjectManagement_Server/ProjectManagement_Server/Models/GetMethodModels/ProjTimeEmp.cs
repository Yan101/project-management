﻿namespace ProjectManagement_Server.Controllers
{
    public class ProjTimeEmp
    {
        public int ID { get; set; }
        public string NameProj { get; set; }
        public int GeneralTime { get; set; }
        public int NumberOfEmployees { get; set; }
    }
}