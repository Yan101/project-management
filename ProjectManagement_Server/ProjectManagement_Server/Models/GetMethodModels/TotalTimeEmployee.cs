﻿namespace ProjectManagement_Server.Models
{
    public class TotalTimeEmployee
    {
        public int ID { get; set; }
        public string FName { get; set; }
        public int TotalTime { get; set; }
    }
}
