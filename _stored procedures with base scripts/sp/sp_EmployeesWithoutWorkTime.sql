USE [ProjectManagement]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sp_EmployeesWithoutWorkTime]

SELECT	'Return Value' = @return_value

GO
