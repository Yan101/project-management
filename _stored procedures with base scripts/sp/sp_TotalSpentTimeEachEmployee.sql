USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_TotalSpentTimeEachEmployee]    Script Date: Ср 14.12.16 19:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_TotalSpentTimeEachEmployee]
AS
SELECT dbo.Employees.ID, dbo.Employees.EmployeeFirstName, SUM(dbo.Project_Employee_SpendTimeEmployee.SpentEmployeeTime) AS 'TotalSpentTimeEachEmployee' FROM dbo.Project_Employee_SpendTimeEmployee, dbo.Employees
WHERE dbo.Employees.ID=dbo.Project_Employee_SpendTimeEmployee.ID_Employee
GROUP BY dbo.Employees.ID, dbo.Employees.EmployeeFirstName
