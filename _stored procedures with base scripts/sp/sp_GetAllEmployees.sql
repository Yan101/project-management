USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllEmployees]    Script Date: Пн 12.12.16 15:58:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_GetAllEmployees]
AS
	SELECT dbo.Employees.ID, dbo.Employees.EmployeeFirstName
	FROM dbo.Employees