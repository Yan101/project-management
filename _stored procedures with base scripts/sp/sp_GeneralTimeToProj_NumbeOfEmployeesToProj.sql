USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralTimeToProj_NumbeOfEmployeesToProj]    Script Date: Ср 14.12.16 18:57:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_GeneralTimeToProj_NumbeOfEmployeesToProj]
AS
	SELECT dbo.Projects.ID, dbo.Projects.ProjectName, SUM(dbo.Project_Employee_SpendTimeEmployee.SpentEmployeeTime) AS 'General time spent on the project', COUNT(DISTINCT dbo.Project_Employee_SpendTimeEmployee.ID_Employee) AS 'Number of employees to each project' FROM dbo.Project_Employee_SpendTimeEmployee, dbo.Projects
	WHERE dbo.Projects.ID = dbo.Project_Employee_SpendTimeEmployee.ID_Project GROUP BY dbo.Projects.ID, dbo.Projects.ProjectName

	