USE [ProjectManagement]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllProjects]    Script Date: Пн 12.12.16 15:58:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_GetAllProjects]
AS
	SELECT dbo.Projects.ID, dbo.Projects.ProjectName 
	FROM dbo.Projects
