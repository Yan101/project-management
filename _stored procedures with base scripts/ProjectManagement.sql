USE [ProjectManagement]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeFirstName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Project_Employee_SpendTimeEmployee]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project_Employee_SpendTimeEmployee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Project] [int] NOT NULL,
	[ID_Employee] [int] NOT NULL,
	[SpentEmployeeTime] [int] NOT NULL,
 CONSTRAINT [PK_Employee_SpendTimeEmployee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Projects]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (1, N'Andey')
INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (2, N'Viktor')
INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (3, N'Egor')
INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (4, N'Petr')
INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (5, N'Vasiliy')
INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (6, N'Igor')
INSERT [dbo].[Employees] ([ID], [EmployeeFirstName]) VALUES (7, N'Vyacheslav')
SET IDENTITY_INSERT [dbo].[Employees] OFF
SET IDENTITY_INSERT [dbo].[Project_Employee_SpendTimeEmployee] ON 

INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (0, 1, 1, 1)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (1, 1, 2, 3)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (2, 1, 3, 3)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (3, 1, 5, 6)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (4, 1, 4, 5)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (5, 2, 4, 2)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (6, 2, 3, 9)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (7, 2, 5, 3)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (9, 2, 1, 3)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (10, 3, 4, 2)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (11, 3, 2, 6)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (12, 3, 1, 2)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (13, 3, 1, 2)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (14, 3, 1, 3)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (15, 3, 1, 1)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (17, 3, 1, 3)
INSERT [dbo].[Project_Employee_SpendTimeEmployee] ([ID], [ID_Project], [ID_Employee], [SpentEmployeeTime]) VALUES (18, 3, 1, 6)
SET IDENTITY_INSERT [dbo].[Project_Employee_SpendTimeEmployee] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([ID], [ProjectName]) VALUES (1, N'Library')
INSERT [dbo].[Projects] ([ID], [ProjectName]) VALUES (2, N'NoteBookStore')
INSERT [dbo].[Projects] ([ID], [ProjectName]) VALUES (3, N'Autoparts')
SET IDENTITY_INSERT [dbo].[Projects] OFF
ALTER TABLE [dbo].[Project_Employee_SpendTimeEmployee]  WITH CHECK ADD  CONSTRAINT [FK_Project_Employee_SpendTimeEmployee_Employees] FOREIGN KEY([ID_Employee])
REFERENCES [dbo].[Employees] ([ID])
GO
ALTER TABLE [dbo].[Project_Employee_SpendTimeEmployee] CHECK CONSTRAINT [FK_Project_Employee_SpendTimeEmployee_Employees]
GO
ALTER TABLE [dbo].[Project_Employee_SpendTimeEmployee]  WITH CHECK ADD  CONSTRAINT [FK_Project_Employee_SpendTimeEmployee_Projects] FOREIGN KEY([ID_Project])
REFERENCES [dbo].[Projects] ([ID])
GO
ALTER TABLE [dbo].[Project_Employee_SpendTimeEmployee] CHECK CONSTRAINT [FK_Project_Employee_SpendTimeEmployee_Projects]
GO
/****** Object:  StoredProcedure [dbo].[sp_EmployeesWithoutWorkTime]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_EmployeesWithoutWorkTime]
AS
SELECT dbo.Employees.EmployeeFirstName
FROM dbo.Employees
WHERE dbo.Employees.ID NOT IN (SELECT dbo.Project_Employee_SpendTimeEmployee.ID_Employee FROM dbo.Project_Employee_SpendTimeEmployee)

GO
/****** Object:  StoredProcedure [dbo].[sp_GeneralTimeToProj_NumbeOfEmployeesToProj]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GeneralTimeToProj_NumbeOfEmployeesToProj]
AS
	SELECT dbo.Projects.ProjectName, SUM(dbo.Project_Employee_SpendTimeEmployee.SpentEmployeeTime) AS 'General time spent on the project', COUNT(DISTINCT dbo.Project_Employee_SpendTimeEmployee.ID_Employee) AS 'Number of employees to each project' FROM dbo.Project_Employee_SpendTimeEmployee, dbo.Projects
	WHERE dbo.Projects.ID = dbo.Project_Employee_SpendTimeEmployee.ID_Project GROUP BY dbo.Projects.ProjectName

	
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllEmployees]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetAllEmployees]
AS
	SELECT dbo.Employees.ID, dbo.Employees.EmployeeFirstName
	FROM dbo.Employees
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllProjects]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetAllProjects]
AS
	SELECT dbo.Projects.ID, dbo.Projects.ProjectName 
	FROM dbo.Projects

GO
/****** Object:  StoredProcedure [dbo].[sp_TotalSpentTimeEachEmployee]    Script Date: Пн 12.12.16 15:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_TotalSpentTimeEachEmployee]
AS
SELECT dbo.Employees.EmployeeFirstName, SUM(dbo.Project_Employee_SpendTimeEmployee.SpentEmployeeTime) AS 'TotalSpentTimeEachEmployee' FROM dbo.Project_Employee_SpendTimeEmployee, dbo.Employees
WHERE dbo.Employees.ID=dbo.Project_Employee_SpendTimeEmployee.ID_Employee
GROUP BY dbo.Employees.EmployeeFirstName

GO
