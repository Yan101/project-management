﻿namespace ProjectManagement_Client.Models
{
    public class AllProjects
    {
        public int ID { get; set; }
        public string ProjectName { get; set; }
    }
}
