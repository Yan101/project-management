﻿using ProjectManagement_Client.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Windows.Forms;
using ProjectManagement_Client.Models;

namespace ProjectManagement_Client
{
    public partial class MainForm : Form
    {
        private const string APP_PATH = "http://localhost:8080/";

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (ServerAvailable())
            {
                Fill_dgv_ProjectEmployees();
                Fill_dgv_AddWorkTimeEmoployee();

                lbl_server_address.ForeColor = System.Drawing.Color.Green;
                lbl_server_address.Text = "Server URL: " + APP_PATH;
            }

            else
            {
                lbl_server_address.Text = "Server URL: " + "not availible";
                lbl_server_address.ForeColor = System.Drawing.Color.Red;

                MessageBox.Show("Server is not available (maybe just run the server?)");
            }
        }
        private void btn_Send_Click(object sender, EventArgs e)
        {
            int ID_Project = Convert.ToInt32(this.dgv_ChooseProject.CurrentRow.Cells[0].Value);
            int ID_Employee = Convert.ToInt32(this.dgv_ChooseEmployee.CurrentRow.Cells[0].Value);
            int WorkTime = Convert.ToInt32(txtbx_WorkTime.Text);

            PostAddWorkTime postAddWorkTime = new PostAddWorkTime();
            postAddWorkTime.ID_Project = ID_Project;
            postAddWorkTime.ID_Employee = ID_Employee;
            postAddWorkTime.SpentEmployeeTime = WorkTime;

            if (ServerAvailable())
            {
                PostRequest<PostAddWorkTime>(postAddWorkTime, "PostAddWorkTimeEmployees");
                
            }
            else
                MessageBox.Show("Server is not available (maybe just run the server?)");
        }

        private static bool ServerAvailable()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(APP_PATH + "api/values/GetAllProjects/").Result;
                    if (response.IsSuccessStatusCode)
                        return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }
        private void Fill_dgv_ProjectEmployees()
        {
            dgv_Projects.DataSource = null;
            dgv_Employees.DataSource = null;
            dgv_EmployeesWithoutWorkTime.DataSource = null;

            dgv_Projects.DataSource = GetRequest<ProjTimeEmp>("GetCommonInfoOnProjects");
            dgv_Employees.DataSource = GetRequest<TotalTimeEmployee>("GetTotalTimeEmployees");
            dgv_EmployeesWithoutWorkTime.DataSource = GetRequest<EmployeesWithoutWorkTime>("GetEmployeesWithoutWorkTime");
        }
        private void Fill_dgv_AddWorkTimeEmoployee()
        {
            dgv_ChooseProject.DataSource = null;
            dgv_ChooseEmployee.DataSource = null;

            dgv_ChooseProject.DataSource = GetRequest<AllProjects>("GetAllProjects");
            dgv_ChooseEmployee.DataSource = GetRequest<AllEmployees>("GetAllEmployees");
        }

        private static List<T> GetRequest<T>(string MethodAddress)
        {
            List<T> tempList = new List<T>();

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = client.GetAsync(APP_PATH + "api/values/" + MethodAddress).Result;
                if (response.IsSuccessStatusCode)
                {
                    string result = response.Content.ReadAsStringAsync().Result;
                    tempList = JsonConvert.DeserializeObject<List<T>>(result);
                }
            }
            return tempList;
        }
        private static void PostRequest<T>(T postAddWorkTime, string MethodAddress)
        {
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage resp = client.PostAsJsonAsync(APP_PATH + "api/values/" + MethodAddress, postAddWorkTime).Result;

                if (resp.IsSuccessStatusCode)
                    MessageBox.Show("Your entry is successfully added!");
                else
                    MessageBox.Show("Data don't receive to server!");
            }
        }
    }
}